redux-sweeper
===

An implementation of Minesweeper using React and Redux.

See demo using [Gitlab Pages](http://quesog.gitlab.io/redux-sweeper/)

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).
