export const CELL_CLICKED = 'CELL_CLICKED'
export const CELL_RIGHT_CLICKED = 'CELL_RIGHT_CLICKED'
export const CELL_MIDDLE_CLICKED = 'CELL_MIDDLE_CLICKED'
export const RESET_CLICKED = 'RESET_CLICKED'

export const SweeperActions = {
  CELL_CLICKED,
  CELL_RIGHT_CLICKED,
  CELL_MIDDLE_CLICKED,
  RESET_CLICKED,
}

export function cellClicked(index) {
  return {
    type: CELL_CLICKED,
    index: index,
  }
}

export function cellRightClicked(index) {
  return {
    type: CELL_RIGHT_CLICKED,
    index: index,
  }
}
export function cellMiddleClicked(index) {
  return {
    type: CELL_MIDDLE_CLICKED,
    index: index,
  }
}
export function resetClicked() {
  return {
    type: RESET_CLICKED,
  }
}
