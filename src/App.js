import React, { Component } from 'react';
import BoardContainer from './containers/BoardContainer'
import ControlsContainer from './containers/ControlsContainer'

import './App.css'

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="parent">
          <div className="upper">
            <ControlsContainer />
          </div>
          <BoardContainer />
        </div>
      </div>
    );
  }
}

export default App;
