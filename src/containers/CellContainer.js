import { connect } from 'react-redux'
import Cell from '../components/Cell'
import { cellClicked, cellRightClicked, cellMiddleClicked } from '../actions'

const mapStateToProps = (state, ownProps) => {
  const cell = {...state.sweeper.cells[ownProps.x][ownProps.y]}
  return { cell }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onLeftClick: (index) => dispatch(cellClicked(index)),
    onRightClick: (index) => dispatch(cellRightClicked(index)),
    onMiddleClick: (index) => dispatch(cellMiddleClicked(index)),
  }
}

const CellContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Cell)

export default CellContainer
