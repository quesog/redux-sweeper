import { connect } from 'react-redux'
import Controls from '../components/Controls'
import { resetClicked } from '../actions'
import { countRemainingMines } from '../utils/boardUtils'

const mapStateToProps = (state) => {
  return {
    timeStarted: state.sweeper.timeStarted,
    remainingMines: state.sweeper.newGame ?
      state.sweeper.settings.mineCount :
      countRemainingMines(state.sweeper.cells)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    resetClicked: () => dispatch(resetClicked()),
  }
}

const ControlsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Controls)

export default ControlsContainer
