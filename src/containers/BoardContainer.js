import { connect } from 'react-redux'
import Board from '../components/Board'

const mapStateToProps = (state) => {
  return {
    rows: state.sweeper.settings.rows,
    cols: state.sweeper.settings.cols,
  }
}

const BoardContainer = connect(
  mapStateToProps
)(Board)

export default BoardContainer
