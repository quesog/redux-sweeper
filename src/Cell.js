export const CellStates = {
  DEFAULT: 'DEFAULT',
  REVEALED: 'REVEALED',
  FLAGGED: 'FLAGGED'
}

export default class Cell {
  constructor({x, y}, isMine) {
    this.x = x
    this.y = y
    this.index = { x, y }
    this.isMine = isMine
    this.state = CellStates.DEFAULT
    this.hintNumber = 0 // must be set later
  }
}
