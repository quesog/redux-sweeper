export function colorForHintNumber(hintNumber) {
  switch (hintNumber) {
    case 1:
      return '#0000ff'
    case 2:
      return '#007f00'
    case 3:
      return '#ff0000'
    case 4:
      return '#00007f'
    case 5:
      return '#7f0000'
    case 6:
      return '#007f7f'
    case 7:
      return '#000000'
    case 8:
      return '#7f7f7f'
    default:
      return '#000000'
  }
}
