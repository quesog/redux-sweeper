import { CellStates } from '../Cell'

export function loopAdjacent(cells, index, callback) {
  const rows = cells.length
  const cols = cells[0].length
  const x = index.x
  const y = index.y

  const adjacents = [ {x: x - 1, y: y - 1},
    {x: x    , y: y - 1},
    {x: x + 1, y: y - 1},
    {x: x - 1, y: y    },
    {x: x + 1, y: y    },
    {x: x - 1, y: y + 1},
    {x: x    , y: y + 1},
    {x: x + 1, y: y + 1}
  ]

  adjacents.filter((adjIndex) => {
    return adjIndex.x >=0 &&
    adjIndex.y >= 0 &&
    adjIndex.x < rows &&
    adjIndex.y < cols
  }).forEach((adjIndex) => {
    callback(adjIndex, cells[adjIndex.x][adjIndex.y])
  })
}

export function loopAll(cells, callback) {
  const rows = cells.length
  const cols = cells[0].length

  for (let x = 0; x < rows; x++) {
    for (var y = 0; y < cols; y++) {
      callback({ x, y }, cells[x][y])
    }
  }
}

export function countRemainingMines(cells) {
  let mineCount = 0
  let flagCount = 0
  loopAll(cells, (index, cell) => {
    mineCount += cell.isMine ? 1 : 0
    flagCount += cell.state === CellStates.FLAGGED ? 1 : 0
  })
  return mineCount - flagCount
}
