import React from 'react'

export default class Timer extends React.Component {
  constructor() {
    super()
    this.state = {
      timeInSeconds: 0
    }
  }

  componentWillReceiveProps({ timeStarted }) {
    if (timeStarted !== undefined) {
      this.interval = setInterval(
        () => this.setState({
          timeInSeconds: Math.floor((new Date() - timeStarted) / 1000)
        }),
        1000)
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  render() {
    return <span>{this.state.timeInSeconds}</span>
  }
}
