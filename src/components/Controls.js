import React from 'react'
import Timer from './Timer'
import './Controls.css'

export default function ({timeStarted, remainingMines, resetClicked}) {
  return (
    <div className="Controls">
      <Timer timeStarted={timeStarted} />
      <button className="ResetButton" onClick={resetClicked}>Reset</button>
      <span className="RemainingMines">{remainingMines}</span>
    </div>
  )
}
