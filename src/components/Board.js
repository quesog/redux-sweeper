import React from 'react'
import range from 'lodash.range'
import CellContainer from '../containers/CellContainer'

import './Board.css'

export default function Board({rows, cols}) {
  return (
    <table className="Board">
      <tbody>
        {range(rows).map(r => (
          <tr className="Row" key={r}>
          {range(cols).map(c => (
            <CellContainer key={`${r},${c}`} x={r} y={c} />
          ))}
          </tr>
        ))}
      </tbody>
    </table>
  )
}
