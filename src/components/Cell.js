import React from 'react'
import './Cell.css'
import { CellStates } from '../Cell'
import { colorForHintNumber } from '../utils/colorUtils'

export default function ({
  cell,
  onLeftClick,
  onRightClick,
  onMiddleClick,
}) {
  let cellText = ''
  let color = ''

  if (cell.state === CellStates.REVEALED) {
    if (cell.isMine) {
      cellText = 'X'
      color = 'red'
    } else {
      cellText = cell.hintNumber === 0 ? ' ' : cell.hintNumber
      color = colorForHintNumber(cell.hintNumber)
    }
  } else if (cell.state === CellStates.FLAGGED) {
    cellText = '\u2691'
  }

  const leftClick = (evt) => {
    evt.preventDefault()
    onLeftClick(cell.index)
  }

  const mouseDown = (evt) => {
    if (evt.nativeEvent.which === 2) {
      evt.preventDefault()
      evt.stopPropagation()
      onMiddleClick(cell.index)
    }
  }

  const rightClick = (evt) => {
    evt.preventDefault()
    onRightClick(cell.index)
  }

  return (
    <td>
      <button
        className={`Cell ${cell.state}`}
        style={{color}}
        onClick={leftClick}
        onMouseDown={mouseDown}
        onContextMenu={rightClick}
      >
      {cellText}
      </button>
    </td>
  )
}
