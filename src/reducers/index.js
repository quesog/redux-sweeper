import { combineReducers } from 'redux'
import sweeper from './sweeper'

export default combineReducers({
  sweeper
})
