import Cell, { CellStates } from '../Cell'
import { SweeperActions } from '../actions'
import { loopAll, loopAdjacent } from '../utils/boardUtils'

function makeBlankCells(settings) {
  const cells = []
  for (let x = 0; x < settings.rows; x++) {
    let row = []
    for (var y = 0; y < settings.cols; y++) {
      row.push(new Cell({x,y}, false))
    }
    cells.push(row)
  }
  return cells
}

function makeCells(settings, initialCellIndex) {
  const randomIntegerInRange = (min, max) =>
    Math.floor(Math.random() * (max - min) + min)
  const locStr = (x,y) => `${x},${y}`

  // precalculate mine locations
  const initialLocStr = locStr(initialCellIndex.x, initialCellIndex.y)
  const mineLocs = new Set()
  while (mineLocs.size < settings.mineCount) {
    let loc = locStr(randomIntegerInRange(0,settings.rows), randomIntegerInRange(0,settings.cols))
    if(initialLocStr !== loc)
      mineLocs.add(loc)
  }

  const cells = []
  for (let x = 0; x < settings.rows; x++) {
    let row = []
    for (var y = 0; y < settings.cols; y++) {
      let cell = new Cell({x,y}, mineLocs.has(locStr(x,y)))
      row.push(cell)
    }
    cells.push(row)
  }

  loopAll(cells, (index, cell) => {
    if (!cell.isMine) {
      let mineCount = 0
      loopAdjacent(cells, index, (innerIndex, innerCell) => {
        mineCount += innerCell.isMine ? 1 : 0
      })
      cell.hintNumber = mineCount
    }
  })
  return cells
}

function initialState() {
  const settings = {
    rows: 10,
    cols: 10,
    mineCount: 20
  }
  const cells = makeBlankCells(settings)

  return {
    newGame: true,
    timeStarted: 0,
    settings,
    cells,
  }
}

function reset(state) {
  return initialState()
}

function cellClicked(state, index) {
  let newState = {...state}
  let targetCell = state.cells[index.x][index.y]

  if (state.newGame) {
    newState.cells = makeCells(newState.settings, targetCell.index)
    targetCell = newState.cells[index.x][index.y]
    newState.newGame = false
    newState.timeStarted = new Date()
  }
  if (targetCell.state === CellStates.DEFAULT) {
    if (targetCell.isMine) {
      loopAll(newState.cells, (i, c) => {
        c.state = CellStates.REVEALED
      })
    } else {
      targetCell.state = CellStates.REVEALED
      if (targetCell.hintNumber === 0) {
        loopAdjacent(state.cells, index, (adjIndex, adjCell) => {
          newState = cellClicked(newState, adjCell)
        })
      }
    }
  }
  return newState
}

function cellRightClicked(state, index) {
  const newCell = {...state.cells[index.x][index.y]}

  if (newCell.state === CellStates.DEFAULT) {
    newCell.state = CellStates.FLAGGED
  } else if (newCell.state === CellStates.FLAGGED) {
    newCell.state = CellStates.DEFAULT
  }
  return {
    ...state,
    cells: state.cells.map((row, x) => {
      return row.map((cell, y) => {
        if (index.x === x && index.y === y) {
          return newCell
        }
        return cell
      })
    })
  }
}

function cellMiddleClicked(state, index) {
  const targetCell = state.cells[index.x][index.y]

  if (targetCell.state !== CellStates.REVEALED)
    return state

  let flaggedCount = 0
  loopAdjacent(state.cells, targetCell.index, (adjIndex, adjCell) => {
    flaggedCount += adjCell.state === CellStates.FLAGGED ? 1 : 0
  })

  if (flaggedCount === targetCell.hintNumber) {
    let newState
    loopAdjacent(state.cells, targetCell.index, (adjIndex, adjCell) => {
      if (adjCell.state === CellStates.DEFAULT) {
        newState = cellClicked(state, adjCell)
      }
    })
    return newState
  }
  return state
}

function sweeper(state = initialState(), action) {
  switch (action.type) {
    case SweeperActions.CELL_CLICKED:
      return cellClicked(state, action.index)
    case SweeperActions.CELL_RIGHT_CLICKED:
      return cellRightClicked(state, action.index)
    case SweeperActions.CELL_MIDDLE_CLICKED:
      return cellMiddleClicked(state, action.index)
    case SweeperActions.RESET_CLICKED:
      return reset(state)
    default:
      return state
  }
}

export default sweeper
