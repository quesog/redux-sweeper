import { createStore as reduxCreateStore } from 'redux';
import reducers from './reducers'

export default function createStore() {
  if (process.env.NODE_ENV === 'production') {
    return reduxCreateStore(reducers)
  }
  return reduxCreateStore(reducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())
}
